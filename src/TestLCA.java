import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestLCA {

	LCA tree;
	
	@BeforeEach
	void setUp() throws Exception {
		tree = new LCA();
	}
	
	/*
	 *              1
	 *            /   \
	 *           2     3
	 *          / \   / \
	 *         4   5 6   7
	 */

	@Test
	@DisplayName("findLCA method")
	void testFindLCA() {
		
		assertEquals(tree.findLCA(4, 7), -1, "Base case: LCA with empty root");

        tree.root = new Node(1);
        tree.root.left = new Node(2);
        tree.root.right = new Node(3);
        tree.root.left.left = new Node(4);
        tree.root.left.right = new Node(5);
        tree.root.right.left = new Node(6);
        tree.root.right.right = new Node(7);

        assertEquals(tree.findLCA(4, 8), -1, "LCA of 1 real Node and 1 non-existent Node");
        assertEquals(tree.findLCA(9, 6), -1, "LCA of 1 non-existent Node and 1 real Node");
        assertEquals(tree.findLCA(9, 10), -1, "LCA of 2 non-existent Nodes");

        assertEquals(tree.findLCA(4, 7), 1, "LCA of left leaf and right leaf");
        assertEquals(tree.findLCA(4, 5), 2, "LCA of two left leaves");
        assertEquals(tree.findLCA(6, 7), 3, "LCA of two right leaves");
	}

}
