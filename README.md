# Lowest Common Ancestor (LCA) #

Lowest Common Ancestor program, implemented using a binary tree structure in Java, with accompanying Java JUnit tests.

The LCA implementation in this project is taken from:

* _https://www.geeksforgeeks.org/lowest-common-ancestor-binary-tree-set-1/_

and is used for education on unit testing.